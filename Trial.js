import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, FlatList } from 'react-native';
import { WebView } from 'react-native-webview';
import Video from 'react-native-video';

const API = 'API_KEY';
const channelID = 'UCXgGY0wkgOzynnHvSEVmE3A';
const result = 10;
var finalURL = `https://www.googleapis.com/youtube/v3/search?key=${API}&channelId=${channelID}&part=snippet,id&order=date&maxResults=${result}`

export default class Trial extends Component {
    static navigationOptions = {
        title: 'Fetch API',
        drawerLabel: 'Trial',
    };

    constructor(props) {
        super(props);
        this.state = {
            resultyt: []
        }

    }

    fetchData = () => {
        fetch(finalURL)
            .then((response) => response.json())
            .then((responseJson) => {
                const resultyt = responseJson.items.map(obj => "https://www.youtube.com/embed/" + obj.id.videoId);
                this.setState({ resultyt: resultyt });
                console.log(this.state.resultyt);


            })
            .catch((error) => {
                console.error(error);
            });

    }

    render() {
        //console.log(finalURL);
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>Hello</Text>
                {
                    this.state.resultyt.map((link, i) => {
                        var frame = <View>
                            <Video source={{ uri: { link } }}   // Can be a URL or a local file.
                                ref={(ref) => {
                                    this.player = ref
                                }}                                      // Store reference
                                onBuffer={this.onBuffer}                // Callback when remote video is buffering
                                onError={this.videoError}
                                key={i}
                                style={styles.backgroundVideo} />
                        </View>
                        return frame;
                    })

                }
                {this.frame}

                <Button
                    onPress={() => this.fetchData()}
                    title="Learn More"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
});
