import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

//My Code
//import { Header } from 'react-native-elements';
import SearchBar from './components/SearchBar';
import AppHeader from './components/Header';
import YTSearch from 'youtube-api-search';
import VideoList from './components/VideoList';
import { ScrollView } from 'react-native-gesture-handler';

const API_KEY = 'API_KEY';

export default class App extends Component {
  state = {
    loading: false,
    videos: [],
  }
  onPressSearch = term => {
    this.searchYT(term);
  }

  searchYT = term => {
    this.setState({ loading: true })
    YTSearch({ key: API_KEY, term }, videos => {
      this.setState({
        loading: false,
        videos

      })
      console.log(videos);
    });
  }
  render() {
    return (
      <View style={{ flex: 1 }} >
        <AppHeader
          HeaderText={"Search Youtube Video"}
        />
        <ScrollView>
          <SearchBar
            loading={this.state.loading}
            onPressSearch={this.onPressSearch}
          />
          <VideoList videos={this.state.videos} />
        </ScrollView>
      </View >
    );
  }
}
const styles = StyleSheet.create({
  ContainerStyles: {
    //marginTop: 10,
    //flex: 1,

    flexDirection: "row",
    backgroundColor: "#ddd",
  },
  TextInputstyles: {
    marginTop: 10,
    width: 200,
    //marginLeft: 80,
    //marginRight: 60,
  },
  Buttonstyles: {
    //flex: 1,
    marginTop: 5,
    marginLeft: '10%',
    marginRight: 10,


  }
})
