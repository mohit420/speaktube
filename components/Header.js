import React, { Component } from 'react';

//My Code
import { Header } from 'react-native-elements';
import { tsPropertySignature } from '@babel/types';

const AppHeader = ({ HeaderText }) => { // props === {HeaderText} => props.HeaderText
    return (
        <Header
            centerComponent={{
                text: HeaderText, style: { color: 'white' }
            }}
            containerStyle={{
                backgroundColor: '#E62117',
                justifyContent: 'space-around',
                height: 60,
                paddingBottom: 5,
                paddingTop: -10,
            }}
        />
    );
};

export default AppHeader;