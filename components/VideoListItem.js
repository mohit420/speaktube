import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

const VideoListItem = ({ video }) => {
    return (
        <View>
            <Image
                style={styles.ImageStyles}
                source={{ uri: video.snippet.thumbnails.medium.url }}

            />
            <Text>{video.snippet.title}</Text>
            <Text>{video.snippet.channelTitle}</Text>
            <Text>{video.snippet.description}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    ImageStyles: {
        alignSelf: 'stretch',
        height: 180,
        //marginTop: '40%',
    },

})

export default VideoListItem;