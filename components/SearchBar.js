import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TextInput } from 'react-native';

//My Code
import { Button } from 'react-native-elements';

class SearchBar extends Component {
    state = {
        term: '',
    }

    render() {
        return (
            <View style={{ flex: 1 }} >
                <View style={styles.ContainerStyles}>
                    <View style={styles.TextInputstyles}>
                        <TextInput
                            placeholder={"Search Video"}
                            value={this.state.value}
                            onChangeText={term => this.setState({ term: term })} // {term} === this.setState{term:term}
                            justifyContent={"center"}
                        />
                    </View>
                    <View style={styles.Buttonstyles}>
                        <Button
                            title={this.props.loading === true ? 'Loading..' : 'Search'}
                            onPress={() => this.props.onPressSearch(this.state.term)}
                        />
                    </View>
                </View>

            </View >
        );
    }
}
const styles = StyleSheet.create({
    ContainerStyles: {
        //marginTop: 10,
        //flex: 1,
        flexDirection: "row",
        backgroundColor: "#ddd",
    },
    TextInputstyles: {
        marginTop: 10,
        width: 200,
        //marginLeft: 80,
        //marginRight: 60,
    },
    Buttonstyles: {
        //flex: 1,
        marginTop: 5,
        marginLeft: '10%',
        marginRight: 10,


    }
})
export default SearchBar;